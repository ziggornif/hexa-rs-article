
use std::sync::Arc;
use domain::ports::api::hello_api::HelloAPI;

pub struct State {
  pub domain: Arc<dyn HelloAPI>,
}
