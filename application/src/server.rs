use std::sync::Arc;

use actix_web::{web::Data, App, HttpServer};
use adapters::hello_inmem::HelloInMemAdapter;
use controllers::hello_rest_controller::hello;
use domain::hello::Hello;
use state::state::State;

pub struct ApplicationServer {
    port: String,
}

impl ApplicationServer {
    pub fn new(port: Option<String>) -> Self {
        let port = match port {
            Some(port) => port,
            None => "8000".to_string(),
        };

        Self { port }
    }

    pub async fn run(&self) -> Result<(), std::io::Error> {
        let listener = format!("0.0.0.0:{}", self.port);
        let server = HttpServer::new(move || {
            // TODO: move this in a loader struct
            let adapter = Arc::new(HelloInMemAdapter::new());
            let domain = Arc::new(Hello::new(adapter));
            let app_state = Data::new(State { domain });
            App::new().app_data(app_state).service(hello)
        })
        .bind(listener)?
        .run();
        println!("Application running on http://localhost:{}", self.port);
        server.await
    }
}
