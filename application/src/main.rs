use application::server::ApplicationServer;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let server = ApplicationServer::new(None);
    server.run().await
}
