use domain::ports::spi::hello_adapter::HelloAdapter;

pub struct HelloInMemAdapter {}

impl HelloInMemAdapter {
    pub fn new() -> Self {
        Self {}
    }
}

impl Default for HelloInMemAdapter {
    fn default() -> Self {
        Self::new()
    }
}

impl HelloAdapter for HelloInMemAdapter {
    fn emit(&self, message: &String) {
        println!("DING - New message {}", message);
    }
}
