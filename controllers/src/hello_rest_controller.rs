use actix_web::{get, web, Responder};
use serde::{Deserialize, Serialize};

use state::state::State;

#[derive(Deserialize)]
pub struct Params {
    username: Option<String>,
}

#[derive(Serialize)]
struct HelloResponse {
    message: String,
}

#[get("/hello")]
pub async fn hello(data: web::Data<State>, query: web::Query<Params>) -> impl Responder {
    let message = data.domain.say_hello(query.username.clone());
    web::Json(HelloResponse { message })
}
