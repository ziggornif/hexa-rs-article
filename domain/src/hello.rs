use std::sync::Arc;

use crate::ports::api::hello_api::HelloAPI;
use crate::ports::spi::hello_adapter::HelloAdapter;

pub struct Hello {
    hello_adapter: Arc<dyn HelloAdapter>,
}

impl Hello {
    pub fn new(hello_adapter: Arc<dyn HelloAdapter>) -> Self {
        Self { hello_adapter }
    }
}

impl HelloAPI for Hello {
    fn say_hello(&self, input: Option<String>) -> String {
        let message = match input {
            Some(x) => format!("Hello {} !", x),
            None => "Hello world !".to_owned(),
        };
        self.hello_adapter.emit(&message);
        message
    }
}
