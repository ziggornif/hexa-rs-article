pub trait HelloAPI: Send + Sync {
    fn say_hello(&self, input: Option<String>) -> String;
}
