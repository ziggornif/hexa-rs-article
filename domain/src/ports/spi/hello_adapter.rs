pub trait HelloAdapter: Send + Sync {
    fn emit(&self, message: &String);
}
